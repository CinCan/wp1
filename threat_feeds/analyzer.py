#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import os, sys, json, re, ipaddress, shutil, socket, time, datetime, math
from io import open
from collections import OrderedDict


if sys.version_info >= (3, 0):
    unicode = str


#age report
from bokeh.io import show, output_notebook
from bokeh.plotting import figure
from bokeh.models import Range1d
from bokeh.embed import components
from bokeh.layouts import row
from jinja2 import Environment, PackageLoader, select_autoescape, FileSystemLoader


data_path = "./data_json/"
output_path = "./output/"
ipdata_path ="./ipdata/"
errors_path = "./errors/"
current_file=""
file_mode = False #saves intermidiate steps to JSON files; slower
max_appearances = 0

def _open_format():
    with open("format.json", encoding='utf-8') as file:
        return(json.load(file))

def write_json(data, file, path='./', indent=None, sort=False):
    if not os.path.exists(path):
        os.makedirs(path)    
    a = json.dumps(data, indent=indent, sort_keys=sort)
    with open(path + file, 'w', encoding='utf-8') as outfile:  
        outfile.write(unicode(a))    

#returns dates in dict: key = feed,  value = array of dates of snapshot files in the path
def sort_file_dates(path): #file name feed_name--%Y-%m-%d--%H-%M-%S.json 
    tdata = _open_format()
    a = dict()
    for d in tdata:
        feed =d["source"].replace(" ", "_")    
        for i in os.listdir(path):
            if  i.startswith(feed) and i.endswith(".json"):
                _, date, _ = i.split('--', 2)
                if feed not in a:
                    a[feed]=[]
                a[feed].append(date)
                a[feed] = sorted(a[feed], key=lambda d: map(int, d.split('-')))
    return a

def get_capture_window(feed=None):

    dates =sort_file_dates(data_path)
    temp = []
    for k,a in dates.iteritems():
       temp = temp + list(set(a) - set(temp))

    temp = sorted(temp, key=lambda d: map(int, d.split('-')))
    #print temp
    
    return temp[0], temp[len(temp)-1]


def data_length1(feed=None):
    start, end = get_capture_window(feed)
    x1 = start.split('-')
    x2 = end.split('-')
    d1 = datetime.date(int(x1[0]),int(x1[1]),int(x1[2]))
    d2 = datetime.date(int(x2[0]),int(x2[1]),int(x2[2]))    
    return ((d2 - d1).days +1)

def dates_between(date1, date2):
    x1 = date1.split('-')
    x2 = date2.split('-')
    d1 = datetime.date(int(x1[0]),int(x1[1]),int(x1[2]))
    d2 = datetime.date(int(x2[0]),int(x2[1]),int(x2[2]))
    a = []
    delta = d2 - d1         # timedelta

    for i in range(delta.days + 1):
        date = (d1 + datetime.timedelta(days=i)).strftime("%Y-%m-%d")
        a.append(date)
    return a


#consider replacing inet_aton which ip_address. inet_aton accepts strings like '4', '4.4' padding with zeros to get valid IP
def is_ipv4(ipv4):
    try:
        #ipaddress.ip_address(ipv4)
        socket.inet_aton(ipv4)
        return True
    except socket.error:
        #except ValueError:
        with open(errors_path + current_file ,'a', encoding='utf-8') as out:      
            out.write(ipv4 + '\n')
        return False




def find_common(a1, a2):
    list_len = min(len(a1),len(a2))
    a3 = []
    i = j = 0
    while i < list_len and j < list_len:
        if socket.inet_aton(a1[i]["ip"]) < socket.inet_aton(a2[j]["ip"]):            
            i += 1
        elif socket.inet_aton(a2[j]["ip"]) < socket.inet_aton(a1[i]["ip"]):
            j += 1
        else:
            a3.append(a1[i])    
            i +=1
            j +=1
    return a3


def count_entries(folder, file):
    with open(folder + file, encoding='utf-8') as f: 
        data = json.load(f)
    n=0 
    for l in data["data"]:
        n=n+1
    return n

# Assumes only one file per date
def get_filename(feed,date):
    for i in os.listdir(data_path):            
        if  i.startswith(feed+"--"+date) and i.endswith(".json"):    
            return i

#NOT USED
def print_size():
    tdata = _open_format()
    k=0
    before = []
    after = []
    for d in tdata:
        feed =d["source"].replace(" ", "_")    
        for i in os.listdir(data_path):
            if  i.startswith(feed) and i.endswith(".json"):
                before.append(count_entries(data_path,i))
                after.append(count_entries(output_path,i))
                print("number of entries: "+ str(before[k]) + "-"+ str(after[k])+ "=" + str(before[k]-after[k]) + " in " + i)
                k=k+1




#  {
#  "Alienvault" : {
#     "dates" : [ {
#         "date": "xxxx-yy-zz",
#         "size": n,
#         "added_since_prev": m,
#         "removed_since_prev": l,
#      },
#        ]
#      "overall" :{
#         "added": i,
#         "removed": k,
#      }
# }
def generate_dynamism_data():
    tdata = _open_format()
    dates = sort_file_dates(data_path)
    data = dict()

    for d in tdata:
        feed =d["source"].replace(" ", "_")    
        if feed not in dates.keys():
            continue
        if "ip" in d["format"]:
            for i in range(len(dates[feed])-1):
                       
                if i == 0:
                    first = get_filename(feed,dates[feed][i])
                    start_date =dates[feed][i] 
                    second = get_filename(feed,dates[feed][i+1])
                    with open(data_path + first, encoding='utf-8') as f: 
                        first_data = json.load(f)
                        ff = first_data
                    with open(data_path + second, encoding='utf-8') as f: 
                        second_data = json.load(f)
                else:
                    first_data = second_data
                    second = get_filename(feed,dates[feed][i+1])
                    with open(data_path + second, encoding='utf-8') as f: 
                        second_data = json.load(f)                
                print second

                a3 = find_common(first_data["data"],second_data["data"])
                old = len(first_data["data"])
                new = len(second_data["data"])
                same = len(a3)  


                if feed not in data.keys():
                    data[feed] = dict()
                    data[feed]["dates"] = []
                if i == 0:
                    t = dict(date=dates[feed][i], size=old, added_since_prev=None, removed_since_prev=None)
                    data[feed]["dates"].append(t)
                t = dict(date=dates[feed][i+1], size=new, added_since_prev=new-same, removed_since_prev=old-same)                    
                data[feed]["dates"].append(t) 


                if i == len(dates[feed])-2:
                    first_data = ff
                    a3 = find_common(first_data["data"],second_data["data"])
                    old = len(first_data["data"])
                    new = len(second_data["data"])
                    same = len(a3)    
                    data[feed]["overall"] = dict(added=new-same, removed=old-same, start_date=start_date, end_date=dates[feed][i+1], start_size = data[feed]["dates"][0]["size"], end_size = data[feed]["dates"][len(data[feed]["dates"])-1]["size"])
              
    data = OrderedDict(sorted(data.items(), key=lambda t: t[0].lower()))

    write_json(data, "dynamism_data.json", output_path, 2)
    return data  







def plot_dynamism(data,feed,agedata=None):
    dates = []
    sizes = []
    added = []
    removed = []

    for d in data[feed]["dates"]:
        #_, date = d["date"].split('-', 1)
        
        dates.append(d["date"])
        sizes.append(d["size"])
        added.append(d["added_since_prev"])
        removed.append(d["removed_since_prev"])
    yrange = max(sizes)*1.2
    p1 = make_histogram("Size", sizes, bin_titles=dates, plot_width=400,plot_height=250, yrange=yrange)
    p2 = make_histogram("Added", added, bin_titles=dates, plot_width=400,plot_height=250, color="Green", yrange=yrange)
    p3 = make_histogram("Removed", removed, bin_titles=dates, plot_width=400,plot_height=250, color="Red", yrange=yrange)
    if agedata:
        p4 = make_histogram("Age distribution" ,  agedata, plot_width=400,plot_height=250, xlabel="age [days]")
    #p1.xaxis.major_label_orientation = "vertical"
   # p2.xaxis.major_label_orientation = "vertical"
   # p3.xaxis.major_label_orientation = "vertical"
    p1.xaxis.major_label_orientation = math.pi/4
    p2.xaxis.major_label_orientation = math.pi/4
    p3.xaxis.major_label_orientation = math.pi/4
    if agedata:
        return row(p1,p2,p3,p4)
    return row(p1,p2,p3)
    
         


def dynamism_report_html(agedata = None):
    tdata = _open_format()
    tdata = sorted(tdata, key=lambda k: k['source'].lower()) 

    env = Environment(
        loader = FileSystemLoader('./templates')#,
       # autoescape=select_autoescape(['html', 'xml'])
    )
    template = env.get_template('report.html')
    titles = OrderedDict()
    plots = OrderedDict()
    #data = generate_dynamism_data()

    with open(output_path + 'dynamism_data.json', encoding='utf-8') as f:
        data = json.load(f)

    for k in tdata:
        feed = k["source"].replace(" ", "_")
        if feed in data.keys():   
            plots[feed] = plot_dynamism(data, feed,agedata[feed])
            titles[feed] = k["source"]



    #for k,v in data.iteritems():
        #plots[k] = plot_dynamism(data, k)
        #titles[k] = k     
    

    script, div = components(plots)
    t = template.render(script=script, my_dict=div, titles=titles)
    with open(output_path + 'dynamism.html', 'w', encoding='utf-8') as outfile:  
        outfile.write(unicode(t))    




def make_histogram(title, data, bin_titles=None, xlabel=None, plot_height = 150, plot_width=260, color="Blue", yrange=None):
    if bin_titles:
        x = bin_titles
    else:
        x = [str(x+1) for x in range(len(data))]

     # Set the x_range to the list of categories above
    p = figure(x_range=x, plot_height=plot_height, plot_width=plot_width, title=title, x_axis_label=xlabel)

     # Categorical values can also be used as coordinates
    p.vbar(x=x, top=data, width=0.5, fill_color=color, line_color=color)

     # Set some properties to make the plot look better
    p.xgrid.grid_line_color = None
    p.y_range.start = 0
    p.y_range.end = yrange
    p.toolbar.logo = None
    p.toolbar_location = None

    return p


def dynamism_report_txt():
    tdata = _open_format()
    tdata = sorted(tdata, key=lambda k: k['source'].lower()) 

    with open(output_path + 'dynamism_data.json', encoding='utf-8') as f:
        data = json.load(f)

    with open(output_path +"dynamism.txt", 'w', encoding='utf-8') as dyn:
        dyn.write(unicode('{0:25}{1}{2:7}{3}{4:7}{5}{6:7}{7}{8:7}'.format("feed", " first file", " ", "         second file", "","","", " ", "") + '\n'))
        for k in tdata:
            feed = k["source"].replace(" ", "_")
            if feed in data.keys():   
                for i in range(1,len(data[feed]["dates"])):
                    old = data[feed]["dates"][i-1]
                    new = data[feed]["dates"][i]
                    old_date = old["date"]
                    new_date = new["date"]
                    old_size = old["size"]
                    new_size = new["size"]
                    added = new["added_since_prev"]
                    removed = new["removed_since_prev"]
                    dyn.write(unicode('{0:25}{1}{2:>7}{3}{4:>7}{5}{6:>7}{7}{8:>7}'.format(feed, " old("+ old_date+ "):", str(old_size), "   new("+ new_date+ "):", str(new_size),"   removed:",str(removed), "   added:", str(added) + '\n')))
                dyn.write(unicode('\n'))
                overall = data[feed]["overall"]
                start_date= overall["start_date"]
                end_date= overall["end_date"]
                start_size = overall["start_size"]
                end_size = overall["end_size"]
                added = overall["added"]
                removed = overall["removed"]

                dyn.write(unicode('{0:25}{1}{2:>7}{3}{4:>7}{5}{6:>7}{7}{8:>7}'.format(feed, " old("+ start_date+ "):", str(start_size), "   new("+end_date + "):", str(end_size),"   removed:",str(removed), "   added:", str(added)) + '\n'))
                dyn.write(unicode('\n'))







#saves data from feeds to following JSON format
# "182.131.17.139": { 
#   "feeds": [
#     {
#       "Blocklist.de_Blocklist": {
#           "dates": [
#           "date": "2019-06-22", 
#           "date": "2019-06-23"
#           ]  
#       }
#     },
#     :
#   ]
# }
def ip_dict():
    tdata = _open_format()    
    dates = sort_file_dates(data_path)
    ipdata = dict()
    for d in tdata:
        feed =d["source"].replace(" ", "_")    
        if feed in dates:
            for date in dates[feed]:
                i= get_filename(feed, date)
                print(i)
                if "ip" in d["format"]:
                    with open(data_path + i, encoding='utf-8') as f:
                        data = json.load(f)
                    for da in data["data"]:
                        if da["ip"] not in ipdata:                        
                            ipdata[da["ip"]] = dict(feeds=[])
                        if not [i for i, d1 in enumerate(ipdata[da["ip"]]["feeds"]) if feed in d1.keys()]:
                            a = dict()
                            a[feed] = dict(dates=[date])
                            ipdata[da["ip"]]["feeds"].append(a)
                        else:
                            index = [i for i, d2 in enumerate(ipdata[da["ip"]]["feeds"]) if feed in d2.keys()]
                            
                            m = ipdata[da["ip"]]["feeds"][index[0]]
                            if date not in m[feed]["dates"]: #HIDAS, keksi parempi
                                m[feed]["dates"].append(date)
                                ipdata[da["ip"]]["feeds"][index[0]] = m 

    if file_mode:
        write_json(ipdata, 'ipdata1.json', ipdata_path)
    else:
        return ipdata

#return subset of ip_dict() containing IPs with multiplicity >= min_feed_count
def multiplicity(min_feed_count,prev,only_changed = False, data = None):
    if file_mode:
        with open(ipdata_path+ 'ipdata' +str(prev)+'.json', encoding='utf-8') as f:  
            data = json.load(f)
    else:
        data = data
    ipdata = dict()


    #find first capture date
    first_dates = []
    dates = sort_file_dates(data_path)
    for k, v in dates.iteritems():
        first_dates.append(v[0])
    first_dates = sorted(first_dates, key=lambda d: map(int, d.split('-')))
    first_date = first_dates[0]

    for ip, v in data.iteritems():
 
        ###remove entries which all feeds have first seen date the first capture date
        if only_changed:
            B = 0
            for a in v["feeds"]:
                if    a[a.keys()[0]]["first_seen"] == first_date:
                    B+=1
            if B == len(v["feeds"]):
                continue
        ###

        if len(v["feeds"]) >=min_feed_count:
            ipdata[ip] = v
    if file_mode:
        write_json(ipdata, 'ipdata'+str(min_feed_count)+'.json', ipdata_path)
    else:
        return ipdata


# adds ip.feed.first_seen and ip.feed.last_seen to ip_dict()
def add_first_seen(min_feed_count, data = None):
    if file_mode:
        with open(ipdata_path+'ipdata' + str(min_feed_count) +'.json', encoding='utf-8') as f:  
            ipdata = json.load(f)
    else:
        ipdata = data
    for ip, ip_v in ipdata.iteritems():
        for i, feed, in enumerate(ip_v["feeds"]):
            feed_key= feed.keys()[0]
            feed[feed_key]["first_seen"] = feed[feed_key]["dates"][0]
            feed[feed_key]["last_seen"] = feed[feed_key]["dates"][len(feed[feed_key]["dates"])-1]
            ipdata[ip]["feeds"][i] = feed
    if file_mode:
        write_json(ipdata, 'ipdata'+str(min_feed_count)+'.json', ipdata_path)
    else:
        return ipdata


# adds ip.feed.ages and ip.feed.appearances to ip_dict()
#ip.feed.ages : [{age: n, first_seen: date, last_seen: date},..]
#ip.feed.appearances = len(ages)
def add_age(min_feed_count, data = None):  
    global max_appearances
    if file_mode:
        with open(ipdata_path+'ipdata' + str(min_feed_count) +'.json', encoding='utf-8') as f:  
            ipdata = json.load(f)
    else:
        ipdata = data

    max_appearances = 0
    z = [0] * 10
    y = [None] * 10
    for ip, ip_v in ipdata.iteritems():
        for i, feed, in enumerate(ip_v["feeds"]):
            feed_key= feed.keys()[0]

            #add first and last seen dates
            feed[feed_key]["first_seen"] = feed[feed_key]["dates"][0]
            feed[feed_key]["last_seen"] = feed[feed_key]["dates"][len(feed[feed_key]["dates"])-1]


            consecutive = True
            first_date = feed[feed_key]["dates"][0]
            y[0] = time.time()###########

            for j in range(len(feed[feed_key]["dates"])-1):
                y[3] = time.time()###########
                x = feed[feed_key]["dates"][j].split('-')
                d1 = datetime.date(int(x[0]),int(x[1]),int(x[2]))
                y[4] = time.time()###########
                z[2] += y[4]-y[3] ###########
                x = feed[feed_key]["dates"][j+1].split('-')
                d2 = datetime.date(int(x[0]),int(x[1]),int(x[2]))
                y[5] = time.time()###########
                z[3] += y[5]-y[4] ###########
                d3 = (d2-d1).days
                y[6] = time.time()###########
                z[4] += y[6]-y[5] ###########
                
                if d3 >1:
                    last_date = feed[feed_key]["dates"][j]
                    x = first_date.split('-')
                    d4 = datetime.date(int(x[0]),int(x[1]), int(x[2]))
                    age = (d1-d4).days +1
                    if not "ages" in feed[feed_key].keys():
                        feed[feed_key]["ages"] = [dict(first_date=first_date,last_date=last_date,age=age)]
                    else:
                        feed[feed_key]["ages"].append(dict(first_date=first_date,last_date=last_date,age=age))
                    first_date = feed[feed_key]["dates"][j+1]
            y[1] = time.time()###########
            z[0] += y[1]-y[0] ###########


            last_date = feed[feed_key]["dates"][len(feed[feed_key]["dates"])-1]            
            age = (datetime.datetime.strptime(last_date, "%Y-%m-%d")-datetime.datetime.strptime(first_date, "%Y-%m-%d")).days +1
            if not "ages" in feed[feed_key].keys():
                feed[feed_key]["ages"] = [dict(first_date=first_date,last_date=last_date,age=age)]
            else:
                feed[feed_key]["ages"].append(dict(first_date=first_date,last_date=last_date,age=age))
            feed[feed_key]["appearances"] = len(feed[feed_key]["ages"])
            if feed[feed_key]["appearances"] > max_appearances:
                max_appearances = feed[feed_key]["appearances"]
            feed[feed_key]["age"] = len(feed[feed_key]["dates"])
            y[2] = time.time()
            z[1] += y[2]-y[1]

            ipdata[ip]["feeds"][i] = feed
            #print feed 
    print z[0]
    print z[1]
    print z[2]
    print z[3]
    print z[4]
    print max_appearances
    if file_mode:
        write_json(ipdata, 'ipdata'+str(min_feed_count)+'.json', ipdata_path)
    else:
        return ipdata

#helper function for timeliness report
# returns dictionary: key = feed, value = number of IPs seen in all snapshots
def num_of_entries_per_feed(min_feed_count= 1, data = None):
    times = dict()
    if file_mode:
        with open(ipdata_path+ 'ipdata' + str(min_feed_count) +'.json', encoding='utf-8') as f:  
            ipdata = json.load(f)
    else:
        ipdata = data
    for ip, ip_v in ipdata.iteritems():    
        for i, feed, in enumerate(ip_v["feeds"]):
            for f in feed.keys():
                if f not in times:
                    times[f] = 0
                times[f] +=1
    return times

#helper function for timeliness report
# returns dictionary: key = feed, value = number of IPs seen first
def times_seen_first(min_feed_count, strict, data = None):
    if file_mode:
        with open(ipdata_path+'ipdata' + str(min_feed_count) +'.json', encoding='utf-8') as f:  
            ipdata = json.load(f)
    else:
        ipdata = data
    times = dict()
    for ip, ip_v in ipdata.iteritems():    
        first_seen = dict()
        for i, feed, in enumerate(ip_v["feeds"]):

            for f in feed.keys():
                if f not in first_seen:
                    first_seen[f] = feed[f]["first_seen"]
        firsts = _return_firsts(first_seen, strict)
        for f in firsts:
            if f not in times:
                times[f] = 0
            times[f] +=1
    return times

# Returns list of feeds which have seen the given IP first
def _return_firsts(data, strict):
    first_seen = ""
    dates = []
    first = []
    for k,v in data.iteritems():
        dates.append(v)
    dates = sorted(dates, key=lambda d: map(int, d.split('-')))

    first_seen = dates[0]

    if strict:
        if dates[0] == dates[1]:
            return first

    for k,v in data.iteritems():
        if v == first_seen:
            first.append(k)
    return first




#Mean of feed snapshots
def feed_average_size(feed):
    dates = sort_file_dates(data_path)
    i=0
    for k, d in enumerate(dates[feed]):
        file = get_filename(feed, d)
        i += count_entries(data_path, file)
    return i/(k+1)

#
# only_changed - when True considers only IPs which have been added to some feed at later date than first data capture date
def timeliness_report(data = None, only_changed = False):
    with open(output_path + "timeliness.txt", 'w', encoding='utf-8') as tim:
        tim.write(unicode('#size - mean of daily snapshot sizes\n'))
        tim.write(unicode('#seen - number of IPs found in at least one of the daily snapshots of given feed which appear in at least i different feeds (in some daily snapshot)\n'))
        tim.write(unicode('#seen first - number of IPs which are not seen earlier in any other feeds\n'))
        tim.write(unicode('#seen first (strict) - number of IPs which are seen at least a day earlier than in any other feed\n'))
        tim.write(unicode('#seen late - number of IPs found at least a day earlier in some other feed\n'))
        feed_size = dict()
        for i in range(2,8):
            tim.write(unicode('# IPs found in at least ' + str(i) + ' feeds') + '\n')
            if file_mode:
                multiplicity(i,i-1, only_changed) 
                data = None
            else:
                data = multiplicity(i,i-1, only_changed, data) #generate subset of data with multiplicity atleast i
             #adds at each ipdata.json[ip][feeds][feed_idx] first_seen and last_seen dates
            output = dict()
            times_seen = num_of_entries_per_feed(i, data) #list number of ip's found in each feed
            times_first_strict = times_seen_first(i, True, data)
            times_first = times_seen_first(i, False, data) #list how many times ip is seen first in a given feed, 2nd argument specifies whether the ip is seen only in one of the feeds
            for k,v in times_seen.iteritems():
                
                if i ==2:   
                    feed_size[k] = feed_average_size(k)
                if k not in output:
                    output[k] = dict(times_seen=times_seen[k])
                if k in times_first.keys():
                    output[k]["times_first"] = times_first[k]
                else:
                    output[k]["times_first"] = 0
                if k in times_first_strict.keys():
                    output[k]["times_first_strict"] = times_first_strict[k]
                else:
                    output[k]["times_first_strict"] = 0
            
            tim.write(unicode('{0:25}{1:>12}{2:>22}{3:>22}{4:>10}{5:>22}{6:>10}{7:>22}{8:>10}'.format('feed','size', 'seen', 'seen first','%', 'seen first (strict)', '%', 'seen late', '%'))+'\n')
            for k in sorted(output.keys()):
                v = output[k]
                tim.write(unicode('{0:25}{1:>12}{2:>22}{3:>22}{4:>10.1f}{5:>22}{6:>10.1f}{7:>22}{8:>10.1f}'.format(k, feed_size[k], v["times_seen"], v["times_first"], v["times_first"]/float(v["times_seen"])*100, v["times_first_strict"], v["times_first_strict"]/float(v["times_seen"])*100, v["times_seen"] - v["times_first"], (v["times_seen"] - v["times_first"])/float(v["times_seen"])*100))+'\n')
            tim.write(unicode('\n'))

def get_age_distribution2(data = None):
    global max_appearances
    data_length = data_length1()
    age = dict()
    if file_mode:
        with open(ipdata_path+ 'ipdata1.json', encoding='utf-8') as f:  
            ipdata = json.load(f)
    else:
        ipdata = data
    for ip, ip_v in ipdata.iteritems():    
        for i, feed, in enumerate(ip_v["feeds"]):
            feed_key= feed.keys()[0]            
            if not feed_key in age.keys():
                #ages[feed_key] = [[[]]]
                #print data_length
                age[feed_key] = [0] * data_length
                #print ages
            age[feed_key][(feed[feed_key]["age"] -1)] +=1
            #print age[feed_key]

    write_json(age, "age_data.json", output_path, 2)                
    return age

def get_age_distribution(data= None):
    global max_appearances
    data_length = data_length1()
    ages = dict()
    age = dict()
    if file_mode:
        with open(ipdata_path+ 'ipdata1.json', encoding='utf-8') as f:  
            ipdata = json.load(f)
    else:
        ipdata = data
    for ip, ip_v in ipdata.iteritems():    
        for i, feed, in enumerate(ip_v["feeds"]):
            feed_key= feed.keys()[0]            
            if not feed_key in ages.keys():
                #ages[feed_key] = [[[]]]

                ages[feed_key] = [None] * max_appearances
                for j in range(max_appearances):
                    ages[feed_key][j] = [None] * (j+1)
                    for k in range(j+1):
                        ages[feed_key][j][k] = [0] * data_length

                #print ages
            for m in range(feed[feed_key]["appearances"]):
                ages[feed_key][(feed[feed_key]["appearances"] -1 )][m][(feed[feed_key]["ages"][m]["age"] -1)] +=1

    write_json(ages, "ages_data.json", output_path, 2)                
    return ages

# "feeds": {
#     "alienvault" : {
#         appearances : [
#             { "1": [ 
#             ]
            
#                 },
#             { "2": [ 
#             ]}

#         ]
#     }
# }
# #



# prints distribution of how many days entries stay in feeds
# consecutive % 
#     


def age_report_html(data, cutoff_percentage):

    env = Environment(
        loader = FileSystemLoader('./templates')#,
       # autoescape=select_autoescape(['html', 'xml'])
    )
    template = env.get_template('age_report.html')

    ages =get_age_distribution(data)
    #age = get_age_distribution2(data) --------------
    appearances = dict()

    s = time.time()
    entries = num_of_entries_per_feed(data=data)
    print str(time.time()-s) + "num_of_entries_per_feed"

    for k,array in ages.iteritems():
        appearances[k] = [0] * max_appearances
        #a[app cnt][appearance][age]
        for i,a in enumerate(array):
            appearances[k][i] = sum(a[0]) 

    data = dict()    
    for k,a in ages.iteritems():
        if not k in data.keys():
            data[k] = dict()   
        data[k]["ages"] = a
        data[k]["appearances"] = appearances[k]
        #data[k]["age"] = age[k] --------------
    #print data
    
    suffix = ['st','nd','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th','th']
    titles = dict()
    plots = dict()
    for k in sorted(data):
        print data[k]["appearances"]
        #titles[k+"_0age"] = k -------------- 
        titles[k+"_0apperances"] = k

        #plots[k+"_0age"] = make_histogram("" , data[k]["age"],xlabel="age [days]") --------------
        plots[k+"_0apperances"] = make_histogram("" , data[k]["appearances"],xlabel="times observed")
        for i, a in enumerate(data[k]["ages"]):
            plotrow = []
            #print i
            for j,b in enumerate(a):
                print data[k]["ages"]
                if sum(b)==0 or float(sum(b))/ entries[k] < cutoff_percentage*0.01: #skip empty datasets or if observation count is less than cutoff_percentage of once observed
                    continue
                plotrow.append(make_histogram(str(j+1)+ suffix[j]+' appearance', b, xlabel="age [days]"))

                #print b
            if len(plotrow) ==0:
                continue
            plots[k +'_'+str(i+1)] = row(plotrow)
            titles[k +'_'+str(i+1)] =  str(i+1) + " times observed"
    print plots
    #plots = {'Red': p1, 'Blue': row(p2,p3)}
    plots = OrderedDict(sorted(plots.items(), key=lambda t: t[0]))
    titles = OrderedDict(sorted(titles.items(), key=lambda t: t[0]))        
    script, div = components(plots)

    
    env = Environment(
        loader = FileSystemLoader('./templates')#,
       # autoescape=select_autoescape(['html', 'xml'])
    )
    template = env.get_template('age_report.html')  
    t = template.render(script=script, my_dict=div, titles=titles)
    with open(output_path +'age_report.html', 'w', encoding='utf-8') as outfile:  
        outfile.write(unicode(t))    






def age_report(data):
    with open(output_path+"age.txt", 'w', encoding='utf-8') as age:
        global max_appearances
        data_length = data_length1()
        age.write(unicode('age\n'))
        ages =get_age_distribution(data)

        ##appearance count distribution
        appearances = dict()
        for k,array in ages.iteritems():
            appearances[k] = [0] * max_appearances
            #a[app cnt][appearance][age]
            for i,a in enumerate(array):
                appearances[k][i] = sum(a[0])
        print appearances
        #print ages


        age.write(unicode("distribution number of appearances\n"))

        for k in sorted(appearances):
            age.write(unicode('{0:25}'.format(k)))
            for d in appearances[k]:
                age.write(unicode('{0:8}'.format(d )))            
            age.write(unicode('\n'))


            

        for i in range(max_appearances):
            age.write(unicode('distributions for '+ str(i+1) + ' appearances\n')) 
            if i== 0:
                age.write(unicode('{0:25}'.format('days')))
                for n in range(data_length):
                    age.write(unicode('{0:8}'.format(n+1)))                          
                age.write(unicode('\n'))

            for j in range(i+1):
                if i>=1:
                    age.write(unicode('age distribution of ' + str(j+1) + 'th apperance\n')) 
                    age.write(unicode('{0:25}'.format('days')))
                    for n in range(data_length):
                        age.write(unicode('{0:8}'.format(n+1)))  
                    age.write(unicode('\n'))

                for feed in  sorted(ages): 
                    dist = ages[feed][i][j]
                    total = 0
                    #for d in dist:
                        #total += d
                    #if total == 0:
                        #continue
                    if sum(dist)==0:
                        continue
                    age.write(unicode('{0:25}'.format(feed)))
                    for d in dist:
                        age.write(unicode('{0:8}'.format(d )))
                    age.write(unicode('\n'))
                age.write(unicode('\n'))                     
            age.write(unicode('\n'))             


## sort age distribution by feed 
## sort age distribution by appearance count
## 
## distribution of age 
## 1 appearance
## feed             age (days) 1   2   3  
## alienvault

# 2 appearances  avg. age of first appearance
# feed            
# alienvault

# 2 appearances avg. age of second appearance
# feed 






#def cleanup():
#    shutil.rmtree(data_path)
#    shutil.rmtree(output_path)
#    shutil.rmtree(ipdata_path)
#    shutil.rmtree(errors_path)


##UNUSED
def generate_html_report(script,divs):
    env = Environment(
        loader = FileSystemLoader('./templates')#,
       # autoescape=select_autoescape(['html', 'xml'])
    )
    template = env.get_template('age_report.html')
    t = template.render(script=script, my_dict=divs)
    with open('age_report.html', 'w', encoding='utf-8') as outfile:  
        outfile.write(unicode(t))    

def main():
    arguments = len(sys.argv) - 1
    position = 1 
    args = [] 
    while (arguments >= position):  
        #print ("parameter %i: %s" % (position, sys.argv[position]))
        args.append(sys.argv[position])
        position = position + 1



    if  len(sys.argv) == 1:
        print "Running all metrics"
        data = ip_dict()
        #data = add_first_seen(1,data)        
        data = add_age(1,data)
        timeliness_report(data, False)
        age_report_html(data, 1)
        age_report(data)


    elif sys.argv[1] == "--dynamism":
        print "Run dynamism"
        generate_dynamism_data()
        dynamism_report_txt()
        dynamism_report_html()
    elif sys.argv[1] == "--age_and_dynamism":
        data = ip_dict()
        data = add_age(1,data)
        agedata = get_age_distribution2(data)
        generate_dynamism_data()
        dynamism_report_txt()
        dynamism_report_html(agedata)        
    elif sys.argv[1] == "--test":
        print "test"
        data_length1()
    else:
        print "Invalid input parameter"


    return

    
    start_time = time.time()
    if file_mode:
        ip_dict() 
        add_first_seen(1)
        s = time.time()
        add_age(1)
        print("%s" % (time.time()-s))
    else:
        data = ip_dict()
        data = add_first_seen(1,data)
        s = time.time()
        data = add_age(1,data)
        print("%s" % (time.time()-s))

    print("--- %s seconds ---" % (time.time() - start_time))
    start_time = time.time()
    if file_mode:
        #timeliness_report(None, False)
        age_report_html(None, 1)
    else:
        
        #timeliness_report(data, False)
        age_report_html(data, 1)
    print("--- %s seconds ---" % (time.time() - start_time))
    #    cleanup()
if __name__ == '__main__':
    main()
