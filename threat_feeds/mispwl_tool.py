#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os, sys, json, re, ipaddress, shutil, socket, time, datetime, math
from io import open
from collections import OrderedDict
from pymispwarninglists import WarningLists
from ipaddress import ip_address, ip_network
if sys.version_info >= (3, 0):
    unicode = str


#age report
from bokeh.io import show, output_notebook
from bokeh.plotting import figure
from bokeh.models import Range1d
from bokeh.embed import components
from bokeh.layouts import row
from jinja2 import Environment, PackageLoader, select_autoescape, FileSystemLoader


mispwl_path = "./misp-warninglists/"
data_path = "./data_json/"
output_path = "./output/"
ipdata_path ="./ipdata/"
errors_path = "./errors/"
current_file=""
file_mode = False #saves intermidiate steps to JSON files; slower
max_appearances = 0

def _open_format():
    with open("format.json", encoding='utf-8') as file:
        return(json.load(file))

def write_json(data, file, path='./', indent=None, sort=False):
    if not os.path.exists(path):
        os.makedirs(path)    
    a = json.dumps(data, indent=indent, sort_keys=sort)
    with open(path + file, 'w', encoding='utf-8') as outfile:  
        outfile.write(unicode(a)) 

#returns dates in dict: key = feed,  value = array of dates of snapshot files in the path
def sort_file_dates(path): #file name feed_name--%Y-%m-%d--%H-%M-%S.json 
    tdata = _open_format()
    a = dict()
    for d in tdata:
        feed =d["source"].replace(" ", "_")    
        for i in os.listdir(path):
            if  i.startswith(feed) and i.endswith(".json"):
                _, date, _ = i.split('--', 2)
                if feed not in a:
                    a[feed]=[]
                a[feed].append(date)
                a[feed] = sorted(a[feed], key=lambda d: list(map(int, d.split('-'))))
    return a

# Assumes only one file per date
def get_filename(feed,date):
    for i in os.listdir(data_path):            
        if  i.startswith(feed+"--"+date) and i.endswith(".json"):    
            return i


def load_warninglists():
    folders = os.listdir(mispwl_path + "/lists/")
    folders = sorted(folders)
    print(folders)
    
    lists = OrderedDict()
    
    for f in folders:
        path = mispwl_path + "lists/" + f +"/list.json"
        #print(path)
        with open(path, encoding='utf-8') as file:
            lists[f] = json.load(file)
    mispwl_format = []
    for k,l in lists.items():

        #if l["type"] == "cidr":
        #print(l["type"] + " " + l["name"])
        print(k + " " + l["type"])
        mispwl_format.append(dict(source=k, type=l["type"], description=l["description"], version=l["version"], matching_attributes=l["matching_attributes"]))

    write_json(mispwl_format, "misp-warninglists.json", indent=2)
    return lists

    #print(lists)

def process_lists(lists):

    for k,l in lists.items():
        for ioc in l["list"]:
            try: #process ipv4 and ipv6 addresses
                result = ip_address(ioc)
                if isinstance(result, ipaddress.IPv4Address):
                    if "ipv4" not in l:
                        l["ipv4"] = []
                    l["ipv4"].append(ioc)
                    #print(str(result) + " ipv4")
                if isinstance(result, ipaddress.IPv6Address):
                    #print(str(result) + " ipv6")
                    if "ipv6" not in l:
                        l["ipv6"] = []
                    l["ipv6"].append(ioc)
                continue
            except ValueError:
                #print(ioc)
                result = ""
                pass
            try: #process ipv4 and ipv6 networks
                result = ip_network(ioc)
                if isinstance(result, ipaddress.IPv4Network):
                    if "ipv4cidr" not in l:
                        l["ipv4cidr"] = []
                    l["ipv4cidr"].append(ioc)
                    #print(str(result) + " ipv4")
                if isinstance(result, ipaddress.IPv6Network):
                    #print(str(result) + " ipv6")
                    if "ipv6cidr" not in l:
                        l["ipv6cidr"] = []
                    l["ipv6cidr"].append(ioc)
                continue
            except ValueError:
                #print(ioc)
                result = ""
                pass
            #try:
            #PROCESS DOMAINS ETC

        #Sort ipv4 
        if "ipv4" in l:
            l["ipv4"] = sorted(l["ipv4"], key=lambda item: socket.inet_aton(item))
        if "ipv4cidr" in l:
            l["ipv4cidr"] = sorted(l["ipv4cidr"], key=lambda item: socket.inet_aton(item.split('/')[0]))        
        lists[k] = l

        write_json(l, "list.json", "./misp_sorted/"+k+"/", indent=2)
    return lists

def find_common_ipv4(a1, a2):
    list_len = min(len(a1),len(a2))
    a3 = []
    i = j = 0
    while i < list_len and j < list_len:
        if socket.inet_aton(a1[i]) < socket.inet_aton(a2[j]["ip"]):            
            i += 1
        elif socket.inet_aton(a2[j]["ip"]) < socket.inet_aton(a1[i]):
            j += 1
        else:
            a3.append(a1[i])    
            i +=1
            j +=1
    return a3

# def find_common_ipv4cidr(a1,a2):
#     list_len = min(len(a1),len(a2))

#     a3 = []
#     i = j = 0
#     while i < list_len and j < list_len:
#         if socket.inet_aton(a1[i]) < socket.inet_aton(a2[j]["ip"]):            
#             i += 1
#         elif socket.inet_aton(a2[j]["ip"]) < socket.inet_aton(a1[i]):
#             j += 1
#         else:
#             a3.append(a1[i])    
#             i +=1
#             j +=1
#     return a3


def find_common_ipv4cidr(a1,a2):
    list_len = min(len(a1),len(a2))

    a3 = []
    i = j = 0
    while i < len(a1) and j < len(a2):
        #print(a1[i])
        #print(a1[i].split('/',1)[0])
        #a = socket.inet_aton(a2[j]["ip"])
        #b = socket.inet_aton(a1[i].split('/',1)[0])
        while j < len(a2) and socket.inet_aton(a2[j]["ip"]) < socket.inet_aton(a1[i].split('/',1)[0]):            
#        while a < b:
            j += 1
  #          a = socket.inet_aton(a2[j]["ip"])
   #         b = socket.inet_aton(a1[i].split('/',1)[0])
        if j < len(a2) and ipaddress.IPv4Address(a2[j]["ip"]) in ipaddress.IPv4Network(a1[i]):
            a3.append(dict(ip=a2[j]["ip"],cidr=a1[i]))
            j += 1
        else:
            i += 1
    return a3


def search(lists):
    tdata = _open_format()    
    dates = sort_file_dates(data_path)
    results=dict()
    plotdata=dict()
    for d in tdata:
        feed =d["source"].replace(" ", "_")    
        if feed in dates:
            if "ip" in d["format"] and feed not in results:
                results[feed] = dict(dates=[])        
                plotdata[feed] = dict(data=dict())  
                plotdata[feed]["names"] = []  

            for date in dates[feed]:
                i= get_filename(feed, date)
                print(i)
                if "ip" in d["format"]:

                    with open(data_path + i, encoding='utf-8') as f:
                        data = json.load(f)
                    matches = []
                    
                    for k,l in lists.items():
                        a3 = []    
                        if "ipv4" in l:
                            #print(l["ipv4"])
                            #print(data["data"])

                            a3 = find_common_ipv4(l["ipv4"],data["data"]) 
                                
                        if "ipv4cidr" in l:
                            a3 = a3+ find_common_ipv4cidr(l["ipv4cidr"],data["data"]) 
                        if "ipv4" in l or "ipv4cidr" in l:
                            
                            
                            if l["name"] not in plotdata[feed]["data"]:
                                plotdata[feed]["names"].append(l["name"])
                                plotdata[feed]["data"][l["name"]] = []
                            plotdata[feed]["data"][l["name"]].append(len(a3))
                            a3 = dict(list=a3,name=l["name"], length=len(a3))
                            matches.append(a3)
    #                print(matches)
                    a = dict()
                    a[date] = matches
                    results[feed]["dates"].append(a)
                    plotdata[feed]["dates"] = dates[feed]
                    plotdata[feed]["data"]["dates"] = dates[feed]

    write_json(plotdata, "mispwl_plot_data.json", "./output/", indent=2)  
    write_json(results, "mispwl_hits.json", "./output/", indent=2)
    return plotdata


def plot_hits(data,feed):
   # fruits = ['Apples', 'Pears', 'Nectarines', 'Plums', 'Grapes', 'Strawberries']
   # names
   # years = ["2015", "2016", "2017"]
    #colors = ["#c9d9d3", "#718dbf", "#e84d60"]
    names = data["names"]
    data = data["data"]
    dates = data["dates"]
    colors = ["red", "blue", "red", "blue","red", "blue","red", "blue","red", "blue","red", "blue","red", "blue","red", "blue"]

    p = figure(x_range=dates, plot_height=300, title="MISP Warning List Hits by date",
               toolbar_location=None, tools="hover", tooltips="$name @dates: @$name")

    p.vbar_stack(names, x='dates', width=0.9,  source=data, color=colors)

    p.y_range.start = 0
    p.x_range.range_padding = 0.1
    p.xaxis.major_label_orientation = math.pi/4
    p.xgrid.grid_line_color = None
    p.axis.minor_tick_line_color = None
    p.outline_line_color = None
    p.legend.location = "top_left"
    p.legend.orientation = "horizontal"

    return p 



def misp_report_html(data):
    tdata = _open_format()
    tdata = sorted(tdata, key=lambda k: k['source'].lower()) 

    env = Environment(
        loader = FileSystemLoader('./templates')#,
       # autoescape=select_autoescape(['html', 'xml'])
    )
    template = env.get_template('report.html')
    titles = OrderedDict()
    plots = OrderedDict()
    #data = generate_dynamism_data()

    #with open(output_path + 'dynamism_data.json', encoding='utf-8') as f:
    #    data = json.load(f)

    for k in tdata:
        feed = k["source"].replace(" ", "_")
        if feed in data.keys():   
            plots[feed] = plot_hits(data[feed], feed)
            titles[feed] = k["source"]




    #for k,v in data.items():
        #plots[k] = plot_dynamism(data, k)
        #titles[k] = k     
    

    script, div = components(plots)
    t = template.render(script=script, my_dict=div, titles=titles)
    with open(output_path + 'misp_report.html', 'w', encoding='utf-8') as outfile:  
        outfile.write(unicode(t))    



def main():


    if  len(sys.argv) == 1:
        lists = load_warninglists()
        lists = process_lists(lists)
        plotdata = search(lists)
        misp_report_html(plotdata)
        
  #  elif sys.argv[1] == "--dynamism":
  
   # else:
     #   print("Invalid input parameter")


    #return
    
if __name__ == '__main__':
    main()
