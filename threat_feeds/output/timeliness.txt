#size - mean of daily snapshot sizes
#seen - number of IPs found in at least one of the daily snapshots of given feed which appear in at least i different feeds (in some daily snapshot)
#seen first - number of IPs which are not seen earlier in any other feeds
#seen first (strict) - number of IPs which are seen at least a day earlier than in any other feed
#seen late - number of IPs found at least a day earlier in some other feed
# IPs found in at least 2 feeds
feed                             size                  seen            seen first         %   seen first (strict)         %             seen late         %
Alienvault_IP_Reputation        58089                 88380                 79435      89.9                  1881       2.1                  8945      10.1
BBcan177_Malicious_IPs           2807                   149                   149     100.0                     1       0.7                     0       0.0
Bad_IPs                        428903                 45660                 45651     100.0                  5558      12.2                     9       0.0
Blocklist.de_Blocklist          33668                113070                103055      91.1                 25808      22.8                 10015       8.9
Brute_Force_Blocker              1518                  2331                  1503      64.5                     8       0.3                   828      35.5
CI_Bad_Guys                     15000                 49356                 38796      78.6                  6333      12.8                 10560      21.4
CnC_IPs                           584                    30                    20      66.7                     1       3.3                    10      33.3
Compromised_IPs                  1522                  2260                  1343      59.4                     0       0.0                   917      40.6
Cridex_IPs                        460                   555                   553      99.6                    96      17.3                     2       0.4
Darklist                         7985                  5889                  5465      92.8                   208       3.5                   424       7.2
Dictionary_SSH_Attacks          52659                 27066                 26828      99.1                   572       2.1                   238       0.9
GreenSnow_Blacklist              2842                 12519                  8487      67.8                  1641      13.1                  4032      32.2
Hancitor_IPs                      724                     4                     4     100.0                     1      25.0                     0       0.0
IPSpamList                         50                   151                   120      79.5                    72      47.7                    31      20.5
Malicious_EXE_IPs                 294                    66                    59      89.4                    23      34.8                     7      10.6
Ransomware_IPs                    336                   338                   337      99.7                     3       0.9                     1       0.3
SSL_BL                             79                    12                    10      83.3                     1       8.3                     2      16.7
Talos_IP_Blacklist               1539                  2284                  2166      94.8                    49       2.1                   118       5.2
Tor_IPs                          6184                 10139                  9695      95.6                  2287      22.6                   444       4.4
Zeus_Bad_IPs                      109                   109                   109     100.0                     0       0.0                     0       0.0
ipsum                          250169                237863                187815      79.0                  5389       2.3                 50048      21.0

# IPs found in at least 3 feeds
feed                             size                  seen            seen first         %   seen first (strict)         %             seen late         %
Alienvault_IP_Reputation        58089                 50318                 41528      82.5                   860       1.7                  8790      17.5
BBcan177_Malicious_IPs           2807                    75                    75     100.0                     1       1.3                     0       0.0
Bad_IPs                        428903                  9233                  9225      99.9                  1146      12.4                     8       0.1
Blocklist.de_Blocklist          33668                 17293                 11236      65.0                  1096       6.3                  6057      35.0
Brute_Force_Blocker              1518                  2331                  1503      64.5                     8       0.3                   828      35.5
CI_Bad_Guys                     15000                 49009                 38491      78.5                  6272      12.8                 10518      21.5
CnC_IPs                           584                     1                     0       0.0                     0       0.0                     1     100.0
Compromised_IPs                  1522                  2254                  1337      59.3                     0       0.0                   917      40.7
Cridex_IPs                        460                     5                     4      80.0                     0       0.0                     1      20.0
Darklist                         7985                  4743                  4321      91.1                   199       4.2                   422       8.9
Dictionary_SSH_Attacks          52659                  8002                  7771      97.1                   421       5.3                   231       2.9
GreenSnow_Blacklist              2842                  7494                  3668      48.9                   277       3.7                  3826      51.1
IPSpamList                         50                    90                    63      70.0                    23      25.6                    27      30.0
Malicious_EXE_IPs                 294                    32                    25      78.1                    13      40.6                     7      21.9
Ransomware_IPs                    336                    50                    50     100.0                     0       0.0                     0       0.0
SSL_BL                             79                     8                     6      75.0                     1      12.5                     2      25.0
Talos_IP_Blacklist               1539                  1334                  1223      91.7                     0       0.0                   111       8.3
Tor_IPs                          6184                  1141                  1092      95.7                    32       2.8                    49       4.3
Zeus_Bad_IPs                      109                    78                    78     100.0                     0       0.0                     0       0.0
ipsum                          250169                 69212                 53184      76.8                   775       1.1                 16028      23.2

# IPs found in at least 4 feeds
feed                             size                  seen            seen first         %   seen first (strict)         %             seen late         %
Alienvault_IP_Reputation        58089                  6183                  4364      70.6                    12       0.2                  1819      29.4
BBcan177_Malicious_IPs           2807                     6                     6     100.0                     0       0.0                     0       0.0
Bad_IPs                        428903                  2686                  2682      99.9                   186       6.9                     4       0.1
Blocklist.de_Blocklist          33668                  7470                  4736      63.4                   380       5.1                  2734      36.6
Brute_Force_Blocker              1518                  2170                  1354      62.4                     1       0.0                   816      37.6
CI_Bad_Guys                     15000                  5803                  3604      62.1                   187       3.2                  2199      37.9
Compromised_IPs                  1522                  2105                  1200      57.0                     0       0.0                   905      43.0
Darklist                         7985                  2464                  2246      91.2                    11       0.4                   218       8.8
Dictionary_SSH_Attacks          52659                  3053                  2882      94.4                    58       1.9                   171       5.6
GreenSnow_Blacklist              2842                  2846                   877      30.8                    12       0.4                  1969      69.2
IPSpamList                         50                    61                    42      68.9                    18      29.5                    19      31.1
Malicious_EXE_IPs                 294                    14                    11      78.6                     2      14.3                     3      21.4
Ransomware_IPs                    336                     2                     2     100.0                     0       0.0                     0       0.0
SSL_BL                             79                     3                     2      66.7                     0       0.0                     1      33.3
Talos_IP_Blacklist               1539                   614                   575      93.6                     0       0.0                    39       6.4
Tor_IPs                          6184                   551                   543      98.5                     0       0.0                     8       1.5
Zeus_Bad_IPs                      109                     3                     3     100.0                     0       0.0                     0       0.0
ipsum                          250169                 10524                  9173      87.2                   188       1.8                  1351      12.8

# IPs found in at least 5 feeds
feed                             size                  seen            seen first         %   seen first (strict)         %             seen late         %
Alienvault_IP_Reputation        58089                  1391                  1058      76.1                     2       0.1                   333      23.9
BBcan177_Malicious_IPs           2807                     2                     2     100.0                     0       0.0                     0       0.0
Bad_IPs                        428903                   892                   892     100.0                    26       2.9                     0       0.0
Blocklist.de_Blocklist          33668                  2498                  1656      66.3                    60       2.4                   842      33.7
Brute_Force_Blocker              1518                  1538                   882      57.3                     1       0.1                   656      42.7
CI_Bad_Guys                     15000                  1286                   815      63.4                    13       1.0                   471      36.6
Compromised_IPs                  1522                  1496                   776      51.9                     0       0.0                   720      48.1
Darklist                         7985                  1192                  1040      87.2                     3       0.3                   152      12.8
Dictionary_SSH_Attacks          52659                  1320                  1201      91.0                    22       1.7                   119       9.0
GreenSnow_Blacklist              2842                  1066                   246      23.1                     1       0.1                   820      76.9
IPSpamList                         50                    16                     4      25.0                     0       0.0                    12      75.0
Malicious_EXE_IPs                 294                     5                     3      60.0                     0       0.0                     2      40.0
Ransomware_IPs                    336                     1                     1     100.0                     0       0.0                     0       0.0
SSL_BL                             79                     1                     1     100.0                     0       0.0                     0       0.0
Talos_IP_Blacklist               1539                   358                   340      95.0                     0       0.0                    18       5.0
Tor_IPs                          6184                   334                   331      99.1                     0       0.0                     3       0.9
ipsum                          250169                  2982                  2795      93.7                    21       0.7                   187       6.3

# IPs found in at least 6 feeds
feed                             size                  seen            seen first         %   seen first (strict)         %             seen late         %
Alienvault_IP_Reputation        58089                   481                   343      71.3                     1       0.2                   138      28.7
Bad_IPs                        428903                   301                   301     100.0                     6       2.0                     0       0.0
Blocklist.de_Blocklist          33668                   818                   587      71.8                    28       3.4                   231      28.2
Brute_Force_Blocker              1518                   802                   364      45.4                     1       0.1                   438      54.6
CI_Bad_Guys                     15000                   469                   312      66.5                     2       0.4                   157      33.5
Compromised_IPs                  1522                   795                   299      37.6                     0       0.0                   496      62.4
Darklist                         7985                   390                   298      76.4                     0       0.0                    92      23.6
Dictionary_SSH_Attacks          52659                   538                   468      87.0                     4       0.7                    70      13.0
GreenSnow_Blacklist              2842                   449                    75      16.7                     0       0.0                   374      83.3
IPSpamList                         50                     5                     2      40.0                     0       0.0                     3      60.0
Malicious_EXE_IPs                 294                     1                     1     100.0                     0       0.0                     0       0.0
Talos_IP_Blacklist               1539                   246                   240      97.6                     0       0.0                     6       2.4
Tor_IPs                          6184                   235                   235     100.0                     0       0.0                     0       0.0
ipsum                          250169                  1018                   949      93.2                     5       0.5                    69       6.8

# IPs found in at least 7 feeds
feed                             size                  seen            seen first         %   seen first (strict)         %             seen late         %
Alienvault_IP_Reputation        58089                   177                   130      73.4                     0       0.0                    47      26.6
Bad_IPs                        428903                   126                   126     100.0                     2       1.6                     0       0.0
Blocklist.de_Blocklist          33668                   250                   174      69.6                    13       5.2                    76      30.4
Brute_Force_Blocker              1518                   309                   120      38.8                     0       0.0                   189      61.2
CI_Bad_Guys                     15000                   171                   123      71.9                     0       0.0                    48      28.1
Compromised_IPs                  1522                   308                    92      29.9                     0       0.0                   216      70.1
Darklist                         7985                   123                    62      50.4                     0       0.0                    61      49.6
Dictionary_SSH_Attacks          52659                   199                   148      74.4                     1       0.5                    51      25.6
GreenSnow_Blacklist              2842                   219                    32      14.6                     0       0.0                   187      85.4
IPSpamList                         50                     3                     2      66.7                     0       0.0                     1      33.3
Talos_IP_Blacklist               1539                   124                   121      97.6                     0       0.0                     3       2.4
Tor_IPs                          6184                   121                   121     100.0                     0       0.0                     0       0.0
ipsum                          250169                   338                   312      92.3                     2       0.6                    26       7.7

